# Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Cloudpasta](#cloudpasta)
  - [Terms of use](#terms-of-use)
    - [What we provide](#what-we-provide)
    - [Plans](#plans)
      - [Instructions](#instructions)
      - [Payment rules](#payment-rules)
        - [One time](#one-time)
        - [Monthly subscription](#monthly-subscription)
      - [Payment via Stripe](#payment-via-stripe)
      - [Alternative payment systems](#alternative-payment-systems)
    - [Quota](#quota)
      - [Going over a quota](#going-over-a-quota)
      - [Downgrading to the free plan](#downgrading-to-the-free-plan)
    - [Payments and account cancellation](#payments-and-account-cancellation)
    - [Disclaimer of warranties and liability](#disclaimer-of-warranties-and-liability)
    - [Importing and exporing data and account deletion](#importing-and-exporing-data-and-account-deletion)
  - [Privacy policy](#privacy-policy)
    - [App limitations](#app-limitations)
    - [Saved data](#saved-data)
    - [Email](#email)
    - [Cookies](#cookies)
    - [Backup files](#backup-files)
  - [Contacts](#contacts)
  - [Other](#other)
    - [Hardware](#hardware)
    - [Advices](#advices)
      - [End to end encryption](#end-to-end-encryption)
      - [Backups](#backups)
  - [Thanks](#thanks)
  - [License](#license)

<!--TOC-->

# Cloudpasta

## Terms of use

### What we provide

Community hosting of [Nextcloud](https://nextcloud.com/) accounts with various
quotas at [Cloudpasta](https://cloudpasta.franco.net.eu.org).

A single instance handles all the accounts.

You can find Nextcloud apps on various app stores.

### Plans

| Name  | Size                  | Price                        | Monthly subscription payment link                   | One time payment link                               |
|-------|-----------------------|------------------------------|-----------------------------------------------------|-----------------------------------------------------|
| Free  | 0 B < size <= 5 GB    | free                         |                                                     |                                                     |
| 50GB  | 0 B < size <= 50 GB   | 1.5 € / month  (18 € / year) | [Stripe](https://buy.stripe.com/cN25n333OdLRazefZ4) | [Stripe](https://buy.stripe.com/7sIcPv9sc5fldLq8wD) |
| 75GB  | 0 B < size <= 75 GB   | 2.0 € / month  (24 € / year) | [Stripe](https://buy.stripe.com/00g7vb33OazF5eUaEM) | [Stripe](https://buy.stripe.com/7sI7vb47S37d7n24gp) |
| 100GB | 0 B < size <= 100 GB  | 2.5 € / month  (30 € / year) | [Stripe](https://buy.stripe.com/6oEeXDeMwfTZ4aQ14e) | [Stripe](https://buy.stripe.com/4gw6r733O6jpazebIT) |

#### Instructions

1. [register](https://cloudpasta.franco.net.eu.org/apps/registration/)
2. use the free plan
3. fill in one of the following forms to upgrade or downgrade

| Changing plan form  |
|---------------------|
| [50GB](https://cloudpasta.franco.net.eu.org/apps/forms/BcdQJfDBftgt4pPW)  |
| [75GB](https://cloudpasta.franco.net.eu.org/apps/forms/RTwF9qztmRAfa3XF)  |
| [100GB](https://cloudpasta.franco.net.eu.org/apps/forms/k9x3aQgXSgR7yyNk) |
| [Free](https://cloudpasta.franco.net.eu.org/apps/forms/44mF52YRPAjzidee)  |

#### Payment rules

##### One time

Payments are valid until the end of a month independently on the subscription date.
This is done to simplify accounting.

For example, if a user subscribes on December 30th he will need to do
a new subscription on January 1st to have access for the whole of January:

| Subscription date | Subscription Validity (included) |
|-------------------|----------------------------------|
| December 30th     | December 31st                    |
| September 1st     | September 30st                   |
| June 15th         | June 30th                        |

If a payment does not arrive on time your account will be downgraded to a free plan.
If your used quota is over 5 GB the [*Downgrading to the free plan*](#downgrading-to-the-free-plan)
conditions apply.

##### Monthly subscription

Once you make a monthly subscription no specific condition applies unless you cancel a subscription.
In this case the same rules as [*One time*](#one-time) apply.

#### Payment via Stripe

When you pay via Stripe please use the same email address you used to register on the Cloudpasta instance.
This way I can upgrade your plan easily.

When you pay via Stripe you accept their [terms](https://stripe.com/checkout/terms) and [privacy policy](https://stripe.com/privacy).

#### Alternative payment systems

I accept payment in various crypto. Please contact me.

### Quota

#### Going over a quota

If you happen to go over the quota you need to use a different plan or delete files.

#### Downgrading to the free plan

If you downgrade to the free plan using more than 5 GB of space your files will be kept for 90 days.
After that, your account will be suspended for 7 days.
If no further action on your part is taken your account and files will be permanently deleted without possibility
of recovery.

At the moment it is not possible to downgrade from one paid plan to another.

### Payments and account cancellation

Once you decide to delete your account your monthly subscription will be deleted. See also the [Payment rules](#payment-rules) section.

### Disclaimer of warranties and liability

In no event shall the administrators be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption)
arising out of the use or inability to use the materials on Cloudpasta platform, even if the administrators has been notified orally or
in writing of the possibility of such damage.
Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages,
these limitations may not apply to you.

### Importing and exporing data and account deletion

You have the freedom and right to export your data and ask for account deletion. Have a look at your user settings:

- [Request data export or account deletion](https://cloudpasta.franco.net.eu.org/settings/user#data-request)
- [Exporting or importing data](https://cloudpasta.franco.net.eu.org/settings/user/user_migration)

If you run the data exporting procedure yourself you will find a `user.nextcloud_export` file in your Nextcloud root.
This file is a zip file: you can download it and unzip it.

Once account deletion is completed all files get deleted without possibility
of recovery.

## Privacy policy

### App limitations

To improve privacy the:

- `Deleted files` [^0]
- `Versions` [^1]

apps have been disabled. This means that once you delete a file
there is no possibility of recovery.

`Activity` [^2] is kept for 7 days.

Sharing between users has been disabled also to prevent service abuse: one user could signup
with different emails and share directories so to easily circumvent the plans' quota.
If you need more space please [roll your own Nextcloud instance](https://nextcloud.com/).

### Saved data

Data that you provided such as files, email, name, etc... is stored in a local database or a local storage.
In general none of this data is sent to 3rd parties except:

- notification emails: see the [email](#email) section
- if you mount external storage (optional)

### Email

At the moment the email provider is Outlook because I do not dispose of a static IP address to setup up
an SMTP server.

### Cookies

This website does NOT use:

- third party cookies
- statistics cookies
- profilation cookies
- marketing cookies

This website uses:

- technical cookies

See the [Nextcloud documentation](https://docs.nextcloud.com/server/latest/admin_manual/gdpr/cookies.html#cookies-stored-by-nextcloud).

### Backup files

After you delete data or your account incremental backup files can continue to
exist for up to 30 days. Encrypted off-line backups can exist for a bit longer.

## Contacts

| Email |
|-------|
| cloudpasta@outlook.com |

## Other

### Hardware

- bandwidth is limited to 500 Mbps down, 150 Mbps up (as an estimate)
- this server is NOT hosted in a data center. Check the IP address of the Cloudpasta instance
- data is redunded using BTRFS RAID 1 or F2FS over mdadm RAID 1 both data, database and other backend services
- daily backups are in place
- [security scan](https://scan.nextcloud.com/results/f789a7a8-c76d-4618-a90c-aa8ef745976c)

### Advices

#### End to end encryption

I STRONGLY SUGGEST to encrypt your files. This way neither the server administrator nor anyone else, except you, can
read your files. One disadvantage of this that you lose all the Nextcloud features:

- file sharing
- media viewing
- etc...

Integrated end to end encryption and server encryption in Nextcloud still seem in a non production stage [^3] [^4] [^5].
That is why I decided not to enable them. There are other possibilities to do this, for example using cryfs.
On Debian GNU/Linux you can do something like this:

1. install and configure the Nextcloud client

   ```shell
   sudo apt-get install nextcloud-desktop
   nextcloud-desktop
   ```

2. install cryfs

   ```shell
   sudo apt-get install cryfs
   ```

3. setup a new encrypted container

   ```shell
   cd ~
   cryfs Nextcloud/my-encrypted-files cryfs-mount-point
   ```

4. follow the instructions
5. edit the data in `~/cryfs-mount-point`
6. once you finish close your encrypted container

   ```shell
   cryfs-unmount ~/cryfs-mount-point
   ```

Another possibility is to use [Cryptomator](https://cryptomator.org/)

#### Backups

This is a small server setup. Do your own backups.

## Thanks

- [Nextcloud](https://nextcloud.com) contributors for the main software
- [nic.eu.org](https://nic.eu.org/) for providing free domains
- [1984.hosting](https://1984.hosting/) for providing free DNS handling
- [Let's Encrypt](https://letsencrypt.org/) for providing free certificate signing

[^0]: https://docs.nextcloud.com/server/24/go.php?to=user-trashbin
[^1]: https://docs.nextcloud.com/server/24/go.php?to=user-versions
[^2]: https://docs.nextcloud.org/server/stable/admin_manual/configuration_server/activity_configuration.html
[^3]: https://www.reddit.com/r/NextCloud/comments/ce7h8b/does_nextcloud_support_end_to_end_encryption/
[^4]: https://docs.nextcloud.com/server/19/user_manual/files/encrypting_files.html
[^5]: https://github.com/nextcloud/server/issues/8546

## License

Some sections of this document have been inspired by [Codeberg's Terms Of Use](https://codeberg.org/Codeberg/org/src/branch/main/TermsOfUse.md)
and [Privacy Policy](https://codeberg.org/Codeberg/org/src/branch/main/PrivacyPolicy.md), licensed under CC BY-SA 4.0.

This text is free to be adapted and remixed under the CC-BY-SA (Attribution-ShareAlike 4.0 International) license.
